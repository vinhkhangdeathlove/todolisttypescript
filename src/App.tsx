import React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import "./App.css";
import ToDoList from "./ToDoList/ToDoList";

const App: React.FC = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/inprogess" element={<ToDoList path="inprogess" />} />
        <Route path="/completed" element={<ToDoList path="completed" />} />
        <Route path="/" element={<ToDoList path="inprogess" />} />
      </Routes>
    </BrowserRouter>
  );
};

export default App;
