import moment from "moment";
import React, { useState } from "react";
import { useDispatch } from "react-redux";
import {
  toggleTodoThunk,
  deleteTodoThunk,
} from "../redux/reducers/ToDoList/todosListSlice";
import { AppDispatch } from "../redux/store";
import { Task } from "../type/type";

interface props {
  task: Task;
  handleSetTask: (task: Task) => void;
}

const ListTask = (props: props) => {
  let { task, handleSetTask } = props;

  const [checked, setChecked] = useState(task.isCompleted);
  const dispatch = useDispatch<AppDispatch>();

  // Toggle a status task
  const handleToggleTodo = () => {
    setChecked(!checked);
    dispatch(
      toggleTodoThunk({
        id: task.id,
        nameTask: task.nameTask,
        isCompleted: !task.isCompleted,
        createAt: task.createAt,
      })
    );
  };

  // Delete a task
  const handleDeleteTodo = () => {
    dispatch(deleteTodoThunk(task.id));
  };

  return (
    <div className="item_task">
      <div>
        <span className="mr-4">
          {moment(task.createAt).format("YYYY-MM-DD")}
        </span>
        <span>{task.nameTask}</span>
      </div>
      <div className="flex space-x-2">
        <button onClick={handleToggleTodo}>
          {checked ? (
            <img
              src="https://frontend.tikicdn.com/_desktop-next/static/img/pdp_revamp_v2/checked.svg"
              alt=""
            />
          ) : (
            <img
              src="https://frontend.tikicdn.com/_desktop-next/static/img/pdp_revamp_v2/unchecked.svg"
              alt=""
            />
          )}
        </button>
        <button
          data-bs-toggle="modal"
          data-bs-target="#modal"
          onClick={() => {
            handleSetTask(task);
          }}
        >
          <i className="fa fa-edit text-green-500" />
        </button>
        <button onClick={handleDeleteTodo}>
          <i className="fa fa-trash-alt text-red-500" />
        </button>
      </div>
    </div>
  );
};

export default ListTask;
