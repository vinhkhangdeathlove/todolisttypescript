import React, { useEffect, useState } from "react";
import "./toDoList.css";
import { useTranslation } from "react-i18next";
import { Link, useLocation } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { todosRemainingSelector } from "../redux/selectors";

import ListTask from "./ListTask";
import AddOrEdit from "../component/Modal/AddOrEdit";
import Search from "../component/Search/Search";
import LanguageSelection from "../component/Selection/LanguageSelection";
import filtersSlice from "../redux/reducers/Filters/filtersSlice";
import { fetchTodos } from "../redux/reducers/ToDoList/todosListSlice";
import FilterDate from "../component/FilterDate/FilterDate";
import Loading from "../component/Loading/Loading";
import { Task } from "../type/type";
import { AppDispatch, RootState } from "../redux/store";

interface props {
  path: string;
}

const initialTask = {
  id: "",
  nameTask: "",
  isCompleted: false,
  createAt: "",
};

const ToDoList = (props: props) => {
  const { t } = useTranslation();
  const dispatch = useDispatch<AppDispatch>();
  const location = useLocation().pathname.slice(1);

  //Loading
  const loading = useSelector((state: RootState) => state.todoList.loading);

  const [task, setTask] = useState<Task>(initialTask);
  const listTask = useSelector(todosRemainingSelector);

  const handleSetTask = (task: Task) => {
    setTask(task);
  };

  useEffect(() => {
    dispatch(fetchTodos());
    dispatch(filtersSlice.actions.statusFilterChange(location));
  }, [dispatch, location]);
  return (
    <>
      {loading ? (
        <Loading />
      ) : (
        <div className="flex justify-center mt-10">
          <div className="w-[500px] p-4 bg-blue-100 rounded-md relative">
            {/* Change languages */}
            <LanguageSelection />

            {/* Add the task */}
            <div className="flex items-center justify-between">
              <button
                className="button_green"
                data-bs-toggle="modal"
                data-bs-target="#modal"
                onClick={() => {
                  handleSetTask(initialTask);
                }}
              >
                {t("Add task")}
              </button>
            </div>

            {/* Search tasks by name */}
            <Search />

            {/* Filter */}
            <FilterDate />

            {/* Tabs the list tasks */}
            <div>
              <ul className="flex space-x-2 text-xl">
                <li className="w-1/2 rounded-t-lg">
                  <Link to="/inprogess">
                    {props.path === "inprogess" ? (
                      <span className="tab_span_active">{t("In progess")}</span>
                    ) : (
                      <span className="tab_span">{t("In progess")}</span>
                    )}
                  </Link>
                </li>
                <li className="w-1/2">
                  <Link to="/completed">
                    {props.path === "completed" ? (
                      <span className="tab_span_active">{t("Completed")}</span>
                    ) : (
                      <span className="tab_span">{t("Completed")}</span>
                    )}
                  </Link>
                </li>
              </ul>
              <div className="bg-white p-4 space-y-2 rounded-b-lg">
                {listTask.map((task: Task) => {
                  return (
                    <ListTask
                      key={task.id}
                      task={task}
                      handleSetTask={handleSetTask}
                    />
                  );
                })}
              </div>
            </div>

            {/* Modal AddOrEdit */}
            <AddOrEdit
              title="Task"
              task={task}
              handleSetTask={handleSetTask}
              initialTask={initialTask}
            />
          </div>
        </div>
      )}
    </>
  );
};

export default ToDoList;
