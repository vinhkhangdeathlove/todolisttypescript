export interface Task {
  id: string;
  nameTask: string;
  isCompleted: boolean;
  createAt: string;
}

export interface DataSelection {
  key: string;
  value: string;
}
