import { createSlice } from "@reduxjs/toolkit";

interface FilterState {
  search: string;
  status: string;
  date: string;
}

const initialState: FilterState = { search: "", status: "inprogess", date: "" };

export default createSlice({
  name: "filters",
  initialState,
  reducers: {
    searchFilterChange: (state, action) => {
      state.search = action.payload;
    },
    statusFilterChange: (state, action) => {
      state.status = action.payload;
    },
    dateFilterChange: (state, action) => {
      state.date = action.payload;
    },
  },
});
