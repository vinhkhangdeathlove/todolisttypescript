import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { toDoListService } from "../../../services/toDoListService";
import { Task } from "../../../type/type";

interface ToDoListState {
  todos: Task[];
  loading: boolean;
}

const initialState: ToDoListState = { todos: [], loading: false };

export default createSlice({
  name: "todoList",
  initialState,
  reducers: {
    addTodo: (state, action) => {
      state.todos.push(action.payload);
    },
    toggleTodoStatus: (state, action) => {
      const currentTodo = state.todos.find(
        (todo) => todo.id === action.payload.id
      );
      if (currentTodo) {
        currentTodo.isCompleted = !currentTodo.isCompleted;
      }
    },
    updateTodoStatus: (state, action) => {
      const currentTodo = state.todos.find(
        (todo) => todo.id === action.payload.id
      );
      if (currentTodo) {
        currentTodo.nameTask = action.payload.nameTask;
        currentTodo.createAt = action.payload.createAt;
      }
    },
    deleteTodo: (state, action) => {
      const index = state.todos.findIndex((todo) => todo.id === action.payload);
      if (index !== -1) {
        state.todos.splice(index, 1);
      }
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchTodos.pending, (state, action) => {
        state.loading = true;
      })
      .addCase(fetchTodos.fulfilled, (state, action) => {
        state.todos = action.payload;
        state.loading = false;
      })
      .addCase(addTodoThunk.pending, (state, action) => {
        state.loading = true;
      })
      .addCase(addTodoThunk.fulfilled, (state, action) => {
        state.todos.push(action.payload);
        state.loading = false;
      })
      .addCase(toggleTodoThunk.pending, (state, action) => {
        state.loading = true;
      })
      .addCase(toggleTodoThunk.fulfilled, (state, action) => {
        const currentTodo = state.todos.find(
          (todo) => todo.id === action.payload.id
        );
        if (currentTodo) {
          currentTodo.isCompleted = !currentTodo.isCompleted;
        }
        state.loading = false;
      })
      .addCase(deleteTodoThunk.pending, (state, action) => {
        state.loading = true;
      })
      .addCase(deleteTodoThunk.fulfilled, (state, action) => {
        const index = state.todos.findIndex(
          (todo) => todo.id === action.payload.id
        );
        if (index !== -1) {
          state.todos.splice(index, 1);
        }
        state.loading = false;
      })
      .addCase(updateTodoThunk.pending, (state, action) => {
        state.loading = true;
      })
      .addCase(updateTodoThunk.fulfilled, (state, action) => {
        const currentTodo = state.todos.find(
          (todo) => todo.id === action.payload.id
        );
        if (currentTodo) {
          currentTodo.nameTask = action.payload.nameTask;
          currentTodo.createAt = action.payload.createAt;
        }
        state.loading = false;
      });
  },
});

export const fetchTodos = createAsyncThunk("todos/fetchTodos", async () => {
  const res = await toDoListService.getListTask();
  return res.data;
});

export const addTodoThunk = createAsyncThunk(
  "todos/addTodoThunk",
  async (newTodo: Task) => {
    const res = await toDoListService.addTask(newTodo);
    return res.data;
  }
);

export const toggleTodoThunk = createAsyncThunk(
  "todos/toggleTodoThunk",
  async (toggleTodo: Task) => {
    const res = await toDoListService.updateTask(toggleTodo);
    return res.data;
  }
);

export const deleteTodoThunk = createAsyncThunk(
  "todos/deleteTodoThunk",
  async (idTodo: string) => {
    const res = await toDoListService.deleteTask(idTodo);
    return res.data;
  }
);

export const updateTodoThunk = createAsyncThunk(
  "todos/updateTodoThunk",
  async (updateTodo: Task) => {
    const res = await toDoListService.updateTask(updateTodo);
    return res.data;
  }
);
