import { createSelector } from "@reduxjs/toolkit";
import moment from "moment";
import { Task } from "../type/type";
import { RootState } from "./store";

export const searchTextSelector = (state: RootState) => state.filters.search;
export const filterStatusSelector = (state: RootState) => state.filters.status;
export const filterDateSelector = (state: RootState) => state.filters.date;
export const todoListSelector = (state: RootState) => state.todoList.todos;

export const todosRemainingSelector = createSelector(
  todoListSelector,
  searchTextSelector,
  filterStatusSelector,
  filterDateSelector,
  (todoList, searchText, status, date) => {
    return todoList.filter((todo: Task) => {
      if (date === "" || date === "Invalid date")
        return (
          todo.nameTask.includes(searchText) &&
          (status === "completed" ? todo.isCompleted : !todo.isCompleted)
        );
      return (
        todo.nameTask.includes(searchText) &&
        (status === "completed" ? todo.isCompleted : !todo.isCompleted) &&
        moment(todo.createAt).format("YYYY-MM-DD") ===
          moment(date).format("YYYY-MM-DD")
      );
    });
  }
);
