import { axiosInstance } from "../Api/axiosInstance";
import { Task } from "../type/type";

export const toDoListService = {
  getListTask() {
    return axiosInstance.get("ToDoList");
  },

  addTask(task: Task) {
    return axiosInstance.post("ToDoList", task);
  },

  updateTask(task: Task) {
    return axiosInstance.put(`ToDoList/${task.id}`, task);
  },

  getTaskById(id: string) {
    return axiosInstance.get(`ToDoList/${id}`);
  },

  deleteTask(id: string) {
    return axiosInstance.delete(`ToDoList/${id}`);
  },
};
