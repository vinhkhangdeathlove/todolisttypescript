import moment from "moment";
import React from "react";
import { useTranslation } from "react-i18next";
import {
  addTodoThunk,
  updateTodoThunk,
} from "../../redux/reducers/ToDoList/todosListSlice";
import { useDispatch } from "react-redux";
import { AppDispatch } from "../../redux/store";
import { Task } from "../../type/type";

interface props {
  title: string;
  task: Task;
  handleSetTask: (task: Task) => void;
  initialTask: Task;
}

const AddOrEdit = (props: props) => {
  let { title, task, handleSetTask, initialTask } = props;
  const { t } = useTranslation();
  const dispatch = useDispatch<AppDispatch>();

  const handleConfirm = () => {
    task.id ? dispatch(updateTodoThunk(task)) : dispatch(addTodoThunk(task));
    handleSetTask(initialTask);
  };

  return (
    <div
      className="modal fade fixed top-0 right-0 hidden w-full h-full overflow-x-hidden overflow-y-auto"
      id="modal"
      tabIndex={-1}
      aria-hidden="true"
    >
      <div className="modal-dialog relative flex flex-col w-full bg-white rounded-md p-4">
        <div className="flex justify-between mb-2">
          <span className="font-semibold text-green-500 text-xl">
            {t(title)}
          </span>
          <button
            type="button"
            className="btn-close w-4 h-4 text-black hover:text-black hover:opacity-75"
            data-bs-dismiss="modal"
          />
        </div>

        <div className="bg-blue-200 p-4 border-2 rounded-md">
          <input
            type="text"
            className="form-control w-full px-3 py-2 text-base font-normal text-gray-700 bg-white border border-solid border-gray-300 rounded-md transition focus:outline-none"
            id="taskInput"
            defaultValue={task.nameTask}
            onChange={(e) => {
              handleSetTask({ ...task, nameTask: e.target.value });
            }}
            placeholder={t("Task")}
          />
          <div>
            <label className="bold">{t("Select date")}: </label>
            <input
              type="date"
              id="date"
              name="date"
              className="text-base mt-4 px-3 py-2 rounded-md"
              min="1900-01-01"
              max="2100-12-31"
              value={task.createAt}
              onChange={(e) =>
                handleSetTask({
                  ...task,
                  createAt: moment(e.target.value).format("YYYY-MM-DD"),
                })
              }
            />
          </div>
          <div className="mt-4 flex justify-end space-x-4">
            <button
              type="button"
              className="button_red"
              data-bs-dismiss="modal"
            >
              {t("Cancel")}
            </button>
            <button
              type="button"
              className="button_blue"
              onClick={() => {
                handleConfirm();
              }}
              data-bs-dismiss="modal"
            >
              {t("Confirm")}
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AddOrEdit;
