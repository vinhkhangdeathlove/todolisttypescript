import { t } from "i18next";
import React, { useState } from "react";
import { DataSelection } from "../../type/type";

interface props {
  data: DataSelection[];
  selectedValue: string;
  handleClick: (selectedItem: DataSelection) => void;
}

const Selection = (props: props) => {
  let { data, selectedValue, handleClick } = props;
  const [isOpen, setIsOpen] = useState(false);

  return (
    <div className="select">
      <input
        className="bg-white rounded-md flex items-center px-3 py-1.5 mb-1 border border-gray-300 focus:outline-none shadow-sm w-[150px]"
        onFocus={(e) => {
          setIsOpen(true);
        }}
        onBlur={(e) => {
          setIsOpen(false);
        }}
        defaultValue={selectedValue}
      />
      {isOpen && (
        <div className="bg-white rounded-md py-1 border border-gray-300 shadow-sm">
          {data.map((selectedItem: DataSelection) => {
            return (
              <div
                key={selectedItem.value}
                className="hover:bg-blue-200 flex px-3 py-1.5"
                onMouseDown={(e) => e.preventDefault()}
                onClick={() => {
                  setIsOpen(false);
                  handleClick(selectedItem);
                }}
              >
                <span className="w-4">{selectedItem.key.toUpperCase()}</span>
                <span className="ml-4">{t(selectedItem.value)}</span>
              </div>
            );
          })}
        </div>
      )}
    </div>
  );
};

export default Selection;
