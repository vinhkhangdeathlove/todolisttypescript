import React from "react";
import { useLocation } from "react-router-dom";
import { DataSelection } from "../../type/type";
import Selection from "./Selection";

const LanguageSelection = () => {
  const languages = [
    { key: "en", value: "English" },
    { key: "fr", value: "French" },
    { key: "vi", value: "Vietnamese" },
  ];

  let key = useLocation().search.slice(-2);
  let currentLanguage = languages.find((item) => item.key === key);

  const handleClick = (selectedItem: DataSelection) => {
    window.location.replace(`?lng=${selectedItem.key}`);
  };

  return (
    <div className="absolute right-4 top-4">
      <Selection
        data={languages}
        selectedValue={currentLanguage?.value || "Language"}
        handleClick={handleClick}
      />
    </div>
  );
};

export default LanguageSelection;
