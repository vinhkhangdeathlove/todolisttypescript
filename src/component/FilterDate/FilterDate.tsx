import { t } from "i18next";
import moment from "moment";
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import filtersSlice from "../../redux/reducers/Filters/filtersSlice";
import { AppDispatch, RootState } from "../../redux/store";

export default function FilterDate() {
  const dispatch = useDispatch<AppDispatch>();
  const dateTextSelector = useSelector(
    (state: RootState) => state.filters.date
  );
  const [filterDate, setFilterDate] = useState<string>(dateTextSelector);

  const handleFilter = () => {
    dispatch(filtersSlice.actions.dateFilterChange(filterDate));
  };

  return (
    <div className="my-4 flex items-center space-x-4">
      <label className="bold">{t("Filter by date")}: </label>
      <input
        type="date"
        id="filterDate"
        name="filterDate"
        className="text-base px-3 py-2 rounded-md"
        min="1900-01-01"
        max="2100-12-31"
        defaultValue={filterDate}
        onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
          setFilterDate(moment(e.target.value).format("YYYY-MM-DD"))
        }
      />
      <button className="button_blue" onClick={handleFilter}>
        {t("Filter")}
      </button>
    </div>
  );
}
