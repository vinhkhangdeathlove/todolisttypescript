import { t } from "i18next";
import React, { useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import filtersSlice from "../../redux/reducers/Filters/filtersSlice";
import { AppDispatch, RootState } from "../../redux/store";

const Search: React.FC = () => {
  const dispatch = useDispatch<AppDispatch>();
  const typingTimeoutRef = useRef<ReturnType<typeof setInterval>>();
  const searchTextSelector = useSelector(
    (state: RootState) => state.filters.search
  );

  const [searchTerm, setSearchTerm] = useState<string>(searchTextSelector);

  const handleOnSearch = (e: React.ChangeEvent<HTMLInputElement>) => {
    let value = e.target.value;
    setSearchTerm(value);
    // use debounce to search
    typingTimeoutRef.current && clearTimeout(typingTimeoutRef.current);
    typingTimeoutRef.current = setTimeout(() => {
      dispatch(filtersSlice.actions.searchFilterChange(value));
    }, 500);
  };

  return (
    <div className="relative mb-4">
      <input
        type="text"
        className="form-control w-full pl-10 py-1.5 text-base font-normal text-gray-700 bg-white border border-solid border-gray-300 rounded-md
        transition m-0 focus:outline-none mt-4"
        id="taskInput"
        value={searchTerm}
        onChange={handleOnSearch}
        placeholder={t("Search")}
      />
      <div className="absolute left-4 top-6">
        <i className="fa fa-search" />
      </div>
    </div>
  );
};
export default Search;
